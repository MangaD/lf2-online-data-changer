# LF2 - Online Data Changer

Modify [LF2](http://lf2.net/) data files online, with syntax highlighting.

To build desktop app use Electron.

*Demo:* http://lf2odc.netlify.com