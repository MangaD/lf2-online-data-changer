define("ace/mode/lf2_dc_highlight_rules",["require","exports","module","ace/lib/oop","ace/mode/text_highlight_rules"], function(require, exports, module) {
"use strict";

    var oop = require("../lib/oop");
    var TextHighlightRules = require("./text_highlight_rules").TextHighlightRules;

    oop.inherits(LF2_DCHighlightRules, TextHighlightRules);

    function LF2_DCHighlightRules() {

        var keywords = (
            "when_clear_goto_phase:|name:|head:|small:|file|walking_frame_rate|walking_speedz|walking_speed|running_frame_rate|" +
			"running_speedz|running_speed|heavy_walking_speedz|heavy_walking_speed|heavy_running_speedz|" +
			"heavy_running_speed|jump_height|jump_distancez|jump_distance|dash_height|dash_distancez|" +
			"dash_distance|rowing_height|rowing_distance|w:|h:|row:|col:|pic:|state:|wait:|next:|dvx:|" +
			"dvy:|dvz:|centerx:|centery:|hit_Ua:|hit_Da:|hit_a:|hit_d:|hit_j:|hit_Fa:|hit_Fj:|hit_Uj:|hit_ja:|x:|y:|" +
			"kind:|weaponact:|attacking:|cover:|hp:|mp:|arest:|vrest:|throwinjury:|injury:|zwidth:|sound:|catchingact:|caughtact:|" +
			"oid:|id:|bound:|music:|times:|ratio:|reserve:|fronthurtact:|backhurtact:|act:|join:|weapon_hp:|weapon_drop_hurt:|weapon_hit_sound:|" +
			"weapon_drop_sound:|weapon_broken_sound:|entry:|fall:|bdefend:|effect:|throwvz:|hurtable:|vaction:|" +
			"aaction:|taction:|decrease:|throwvy:|throwvx:|action:|facing:|dircontrol:"
        );

        var labels = (
            "bpoint:|bpoint_end:|cpoint:|cpoint_end:|wpoint:|wpoint_end:|bdy:|bdy_end:|itr:|itr_end:"
        );
        
        var framenames = (
        	"broken_defend|catching|crouch2|crouch|dash_attack|dash_weapon_atck|dash_sword|dash|defend|falling|ground_fire|fire|dflying|uflying|monk_flying|louis_flying|flying|" +
        	"heavy_obj_run|heavy_obj_walk|heavy_stop_run|heavy_weapon_thw|hiting_ground|hitting|hiting|hit|injured|jump_weapon_atck|" +
        	"light_weapon_thw|jump_attack|jump_sword2|jump_sword1|jump_sword|jump|lying|normal_weapon_atck|picked_caught|picking_heavy|" +
        	"picking_light|many_punch|Down_punch|super_punch|punch2|rebounding|rowing|run_attack|run_weapon_atck|running|sky_lgt_wp_thw|standing|" +
        	"throw_lying_man|tired|walking|weapon_drink|stop_running|blastpush|flute|super_arrow|" +
        	"5_arrow|dummy|normal|burn_run|run|in_the_sky|on_hand|throwing|just_on_on_ground|just_on_ground|on_ground|chase_ball|ball34|" +
        	"ball1|ball2|ball3|ball4|ballend|heal_self|heal_other|bat|louis_start|monk_start|start|sstone1|sstone2|sstone3|sstone4|stone1|stone2|" +
        	"stone3|stone4|sstick1|sstick2|sstick3|sstick4|stick1|stick2|stick3|stick4|s_hoe1|s_hoe2|s_hoe3|" +
        	"s_hoe4|k_hoe1|k_hoe2|k_hoe3|k_hoe4|hoe1|hoe2|hoe3|hoe4|box01|box02|box03|box04|box11|box12|box13|" +
        	"box14|box21|box22|box23|box24|box31|box32|box33|box34|baseball1|baseball2|baseball3|baseball4|" +
        	"milk13|milk23|milk33|milk43|milk1|milk2|milk3|milk4|disappearing|disappear|ice11|ice21|ice31|ice41|burning_smoke|" +
        	"beer1|beer2|beer3|beer4|<1|<2|<3|<4|armour1|armour2|armour3|armour4|ice|monk|mark|jack|sorcerer|" +
        	"bandit|hunter|jan|come_here|singlong|blast2|blast3|blast|many_foot|c_foot|stay|move|join|flame|explosion|" +
        	"disaster1|disaster2|disaster|volcano|mix|tail|whirlwind|Whirlwind|Sword|column_mother|column|rodulf_smoke|" +
        	"star2|stars|star|DAA_action1|DAA_action2|DAA_action3|DAA_action4|DA_action2|DA_action3|DA_action4|DA_action5|DA_action|" +
        	"force_field|forcefield_dp|forcefield|biscuit|healball|ball_end|big_ball|ball|borning|super|shadow|col|punch|sword|" +
        	"1000foot|c-throw|r-catch|transform_b|transform|up_spear|air_push|\\+man|fly_crash|cleg|teleport"
        );

        var variableLanguage = (
            "window|arguments|prototype|document"
        );

		// regexp must not have capturing parentheses. Use (?:) instead.
		// regexps are ordered -> the first match is used
        this.$rules = {
            start : [
                {
                    token : "comment",
                    regex : "#.*"
                }, {
                    token : "entity.name.tag",
                    regex : "<.*>"
                }, {
					token : "text",
					regex : "[^\\s]*\\.(bmp|wav|wma|dat)"
                }, {
					token : "storage",
					regex : labels
                }, {
					token : "variable",
					regex : keywords
                }, {
					token : "keyword",
					regex : framenames
                }, {
                    token : "constant.numeric",
                    regex : "(?:\\d+(?:\\.\\d+)?|\\.\\d+)"
                }, {
                    token : "text",
                    regex : "\\s+"
                }],


            heregex : [{
                token : "comment.regex",
                regex : "\\s+(?:#.*)?"
            }],
        };
        this.normalizeRules();
    }

    exports.LF2_DCHighlightRules = LF2_DCHighlightRules;
});

define("ace/mode/matching_brace_outdent",["require","exports","module","ace/range"], function(require, exports, module) {
"use strict";

var Range = require("../range").Range;

var MatchingBraceOutdent = function() {};

(function() {

    this.checkOutdent = function(line, input) {
        if (! /^\s+$/.test(line))
            return false;

        return /^\s*\}/.test(input);
    };

    this.autoOutdent = function(doc, row) {
        var line = doc.getLine(row);
        var match = line.match(/^(\s*\})/);

        if (!match) return 0;

        var column = match[1].length;
        var openBracePos = doc.findMatchingBracket({row: row, column: column});

        if (!openBracePos || openBracePos.row == row) return 0;

        var indent = this.$getIndent(doc.getLine(openBracePos.row));
        doc.replace(new Range(row, 0, row, column-1), indent);
    };

    this.$getIndent = function(line) {
        return line.match(/^\s*/)[0];
    };

}).call(MatchingBraceOutdent.prototype);

exports.MatchingBraceOutdent = MatchingBraceOutdent;
});

define("ace/mode/folding/lf2_dc",["require","exports","module","ace/lib/oop","ace/mode/folding/fold_mode","ace/range"], function(require, exports, module) {
"use strict";

var oop = require("../../lib/oop");
var BaseFoldMode = require("./fold_mode").FoldMode;
var Range = require("../../range").Range;

var FoldMode = exports.FoldMode = function() {};
oop.inherits(FoldMode, BaseFoldMode);

(function() {

    this.getFoldWidgetRange = function(session, foldStyle, row) {
        var range = this.indentationBlock(session, row);
        if (range)
            return range;

        var re = /\S/;
        var line = session.getLine(row);
        var startLevel = line.search(re);
        if (startLevel == -1 || line[startLevel] != "#")
            return;

        var startColumn = line.length;
        var maxRow = session.getLength();
        var startRow = row;
        var endRow = row;

        while (++row < maxRow) {
            line = session.getLine(row);
            var level = line.search(re);

            if (level == -1)
                continue;

            if (line[level] != "#")
                break;

            endRow = row;
        }

        if (endRow > startRow) {
            var endColumn = session.getLine(endRow).length;
            return new Range(startRow, startColumn, endRow, endColumn);
        }
    };
    this.getFoldWidget = function(session, foldStyle, row) {
        var line = session.getLine(row);
        var indent = line.search(/\S/);
        var next = session.getLine(row + 1);
        var prev = session.getLine(row - 1);
        var prevIndent = prev.search(/\S/);
        var nextIndent = next.search(/\S/);

        if (indent == -1) {
            session.foldWidgets[row - 1] = prevIndent!= -1 && prevIndent < nextIndent ? "start" : "";
            return "";
        }
        if (prevIndent == -1) {
            if (indent == nextIndent && line[indent] == "#" && next[indent] == "#") {
                session.foldWidgets[row - 1] = "";
                session.foldWidgets[row + 1] = "";
                return "start";
            }
        } else if (prevIndent == indent && line[indent] == "#" && prev[indent] == "#") {
            if (session.getLine(row - 2).search(/\S/) == -1) {
                session.foldWidgets[row - 1] = "start";
                session.foldWidgets[row + 1] = "";
                return "";
            }
        }

        if (prevIndent!= -1 && prevIndent < indent)
            session.foldWidgets[row - 1] = "start";
        else
            session.foldWidgets[row - 1] = "";

        if (indent < nextIndent)
            return "start";
        else
            return "";
    };

}).call(FoldMode.prototype);

});

define("ace/mode/lf2_dc",["require","exports","module","ace/mode/lf2_dc_highlight_rules","ace/mode/matching_brace_outdent","ace/mode/folding/lf2_dc","ace/range","ace/mode/text","ace/worker/worker_client","ace/lib/oop"], function(require, exports, module) {
"use strict";

var Rules = require("./lf2_dc_highlight_rules").LF2_DCHighlightRules;
var Outdent = require("./matching_brace_outdent").MatchingBraceOutdent;
var FoldMode = require("./folding/lf2_dc").FoldMode;
var Range = require("../range").Range;
var TextMode = require("./text").Mode;
var WorkerClient = require("../worker/worker_client").WorkerClient;
var oop = require("../lib/oop");

function Mode() {
    this.HighlightRules = Rules;
    this.$outdent = new Outdent();
    this.foldingRules = new FoldMode();
}

oop.inherits(Mode, TextMode);

(function() {
    var indenter = /(?:[:])/;
    
    this.lineCommentStart = "#";
    
    this.getNextLineIndent = function(state, line, tab) {
        var indent = this.$getIndent(line);
        var tokens = this.getTokenizer().getLineTokens(line, state).tokens;
    
        if (!(tokens.length && tokens[tokens.length - 1].type === 'comment') &&
            state === 'start' && indenter.test(line))
            indent += tab;
        return indent;
    };
    
    this.checkOutdent = function(state, line, input) {
        return this.$outdent.checkOutdent(line, input);
    };
    
    this.autoOutdent = function(state, doc, row) {
        this.$outdent.autoOutdent(doc, row);
    };

    this.$id = "ace/mode/lf2_dc";
}).call(Mode.prototype);

exports.Mode = Mode;

});
